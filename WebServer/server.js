var express = require("express");
var bodyParser = require("body-parser");
var mongodb = require("mongodb");
var ObjectID = mongodb.ObjectID;
var dayjs = require('dayjs');

var USER_COLLECTION = "user";
var ACCOUNT_COLLECTION = "account";
var TRANSACTION_COLLECTION = "transaction";

var app = express();
app.use(bodyParser.json());

// Create a database variable outside of the database connection callback to reuse the connection pool in the app.
var db;
var dbUri = "mongodb://root:root1234@ds111876.mlab.com:11876/data-xchange";

// Connect to the database before starting the application server.
mongodb.MongoClient.connect(dbUri, function (err, client) {
   if (err) {
      console.log(err);
      process.exit(1);
   }

   // Save database object from the callback for reuse.
   db = client.db();
   console.log("Database connection ready");

   // Initialize the app.
   var server = app.listen(process.env.PORT || 8080, function () {
      var port = server.address().port;
      console.log("App now running on port", port);
   });
});


// Generic error handler used by all endpoints.
function handleError(res, reason, message, code) {
   console.log("ERROR: " + reason);
   res.status(code || 500).json({ "error": message });
}


app.get("/api/user", function (req, res) {
   db.collection(USER_COLLECTION).find({}).toArray(function (err, docs) {
      if (err) {
         handleError(res, err.message, "Failed to get user.");
      } else {
         res.status(200).json(docs);
      }
   });
});

app.post("/api/login", function (req, res) {
   if (!req.body.username) {
      handleError(res, "Invalid user input", "Must provide a name.", 400);
   } else if (!req.body.password) {
      handleError(res, "Invalid user input", "Must provide a password.", 400);
   } else {
      db.collection(USER_COLLECTION).findOne({ username: req.body.username, password: req.body.password }, function (err, doc) {
         if (err) {
            handleError(res, err.message, "Failed to login.");
         } else {
            if (doc) {
               delete doc["password"];
               res.status(201).json(doc);
            }
            else {
               handleError(res, "Login Failed", "Incorrect Username or Password .");
            }
         }
      });
   }
});


app.get("/api/solde/:token", function (req, res) {
   var result = {};
   db.collection(ACCOUNT_COLLECTION).findOne({ idUser: new ObjectID(req.params.token) }, function (err, doc) {
      if (err) {
         handleError(res, err.message, "Failed to get sold");
         return;
      } else {
         result = doc;
         db.collection(TRANSACTION_COLLECTION).find({ idUser: new ObjectID(req.params.token) }).toArray(function(err, docs) { 
            console.log(docs);
            result.transaction = docs;
            res.status(200).json(result);
         });
      }
      
   });
});

app.post("/api/checkSolde/:token", function (req, res) {
   if (!req.body.amount) {
      handleError(res, "Invalid amount input", "Must provide an amount.", 400);
   } else {
      db.collection(ACCOUNT_COLLECTION).findOne({ idUser: new ObjectID(req.params.token) }, function (err, doc) {
         if (err) {
            handleError(res, err.message, "Failed to get solde");
         } else {
            var status = "KO";

            if (doc.solde >= Number(req.body.amount)) {
               status = "OK"
            }

            res.status(200).json({ status: status });
         }
      });
   }
});

function updateSolderUser(userId, solde) {
   db.collection(ACCOUNT_COLLECTION).updateOne({ idUser: new ObjectID(userId) }, { $set: { solde: solde } }, function (err, doc) {
      if (err) {
         return false;
      } else {
         return true;
      }
   });
}

app.post("/api/processTransaction/:token", function (req, res) {
   var amount = req.body.amount;
   if (!req.body.receiverId) {
      handleError(res, "Invalid amount input", "Must provide an receiverId.", 400);
   } else if (!amount) {
      handleError(res, "Invalid amount input", "Must provide an amount.", 400);
   } else if (amount && Number(amount) < 0) {
      handleError(res, "Invalid amount input", "Amount must be positive.", 400);
   } else {
      db.collection(ACCOUNT_COLLECTION).findOne({ idUser: new ObjectID(req.params.token) }, function (err, doc) {
         newSolde = doc.solde - amount;
         if (Number(doc.solde) - Number(amount) < 0) {
            handleError(res, "Invalid amount input", "Cannot process operation: insufficient balance .", 400);
         } else {
            updateSolderUser(req.params.token, doc.solde - amount);
            var newTransactionSender = {
               "idUser": new ObjectID(req.params.token),
               "date": ""+dayjs().format('YYYY-MM-DD HH:mm'),
               "montant": amount*-1,
               "label": "Transfert vers l'utilisateur: "+req.body.receiverId
            }
            db.collection(TRANSACTION_COLLECTION).insertOne(newTransactionSender, function (err, doc) {});

            db.collection(ACCOUNT_COLLECTION).findOne({ idUser: new ObjectID(req.body.receiverId) }, function (err, doc) {
               updateSolderUser(req.body.receiverId, doc.solde + amount);
               var newTransactionReceiver = {
                  "idUser": new ObjectID(req.body.receiverId),
                  "date": ""+dayjs().format('YYYY-MM-DD HH:mm'),
                  "montant": amount,
                  "label": "Reception de l'utilisateur: "+req.params.token
               }
               db.collection(TRANSACTION_COLLECTION).insertOne(newTransactionReceiver, function (err, doc) {});
            });
            
            res.status(200).json({ status: "success", amount: amount });
         }
      });
   }
});
package com.mbds.android.nfc.code.ui.home;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.mbds.android.nfc.code.Constant;
import com.mbds.android.nfc.code.R;
import com.mbds.android.nfc.code.model.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;
    private TextView profilename;
    private TextView address;
    private TextView currentSolde;
    private TextView accountNumber;
    private final String currency = "€";

    SharedPreferences sharedpreferences;
    private RequestQueue queue;

    public View onCreateView(@NonNull LayoutInflater inflater,
            ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_account, container, false);
        profilename = root.findViewById(R.id.profile_name);
        currentSolde = root.findViewById(R.id.profile_balance);
        accountNumber = root.findViewById(R.id.profile_number);
        address = root.findViewById(R.id.profile_location);
        sharedpreferences = this.getActivity().getSharedPreferences(Constant.MyPREFERENCES, Context.MODE_PRIVATE);

        profilename.setText(sharedpreferences.getString(Constant.UserName,null));
        address.setText(sharedpreferences.getString(Constant.Address,null));
        queue = Volley.newRequestQueue(getContext());
        getSolde(sharedpreferences.getString(Constant.ID,null));
        return root;
    }
    public void getSolde(String userId){
        String url = "https://data-xchange.herokuapp.com/api/solde/"+userId;
        User user= null;
        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.GET, url,null,
                new Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject responseJsonObject) {
                        // response
                        try {
                            int solde=(int)responseJsonObject.getInt("solde");
                            String accountId = (String)responseJsonObject.getString("_id");
                            accountNumber.setText(accountId);
                            currentSolde.setText(Integer.toString(solde) +" "+currency);

                        } catch (JSONException e) {
                            Log.v("JSonErr", e.getMessage());

                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        error.printStackTrace();
                        Log.e("Error.Response", String.valueOf(error.networkResponse.statusCode));
                    }
                }
        ) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/json");
                return params;
            }
        };
        queue.add(postRequest);

    }
}

package com.mbds.android.nfc.code.ui.dashboard;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.mbds.android.nfc.code.Constant;
import com.mbds.android.nfc.code.NDEFPushActivity;
import com.mbds.android.nfc.code.R;
import com.mbds.android.nfc.code.model.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class DashboardFragment extends Fragment {

    private DashboardViewModel dashboardViewModel;
    private EditText money;
    private Button sendButton;
    private String userId;
    SharedPreferences sharedpreferences;
    private RequestQueue queue;
    public View onCreateView(@NonNull LayoutInflater inflater,
            ViewGroup container, Bundle savedInstanceState) {
        dashboardViewModel =
                ViewModelProviders.of(this).get(DashboardViewModel.class);
        View root = inflater.inflate(R.layout.fragment_transfer, container, false);
        money = root.findViewById(R.id.send_money_value);
        sendButton = root.findViewById(R.id.send);
        queue = Volley.newRequestQueue(getContext());

        sendButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // your handler code here
                String amount =money.getText().toString();
                checkSold(amount);

            }
        });
        sharedpreferences =getActivity().getSharedPreferences(Constant.MyPREFERENCES, Context.MODE_PRIVATE);

        userId = sharedpreferences.getString(Constant.ID,null);

        return root;
    }
    public void checkSold(String amount) {
        String url = "https://data-xchange.herokuapp.com/api/checkSolde/" + userId;
        User user = null;
        Map<String, String> params = new HashMap<String, String>();
        params.put("amount", amount);
        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject responseJsonObject) {
                        // response
                        try {
                            String status = (String) responseJsonObject.get("status");
                            if (status.compareTo("OK") == 0) {
                                Intent intent = new Intent(getActivity(), NDEFPushActivity.class);
                                intent.putExtra("amount", money.getText().toString());
                                startActivity(intent);
                            } else {
                                Toast toast = Toast.makeText(getContext(), "Please Check your balance", Toast.LENGTH_LONG);
                                toast.show();
                            }
                        } catch (JSONException e) {
                            Log.v("JSonErr", e.getMessage());

                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        error.printStackTrace();
                        Log.e("Error.Response", String.valueOf(error.networkResponse.statusCode));
                    }
                }
        ) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                return params;
            }
        };
        queue.add(postRequest);


    }
}

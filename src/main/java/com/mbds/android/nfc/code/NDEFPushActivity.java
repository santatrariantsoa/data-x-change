package com.mbds.android.nfc.code;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.NfcEvent;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.mbds.android.nfc.code.model.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.Console;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static android.nfc.NdefRecord.createMime;

public class NDEFPushActivity extends Activity implements NfcAdapter.CreateNdefMessageCallback, NfcAdapter.OnNdefPushCompleteCallback {

    private NfcAdapter nfcAdapter;
    private NdefRecord ndefRecord;
    private NdefMessage ndefMessage;
    SharedPreferences sharedpreferences;
    private String userId;
    private String amount;
    private RequestQueue queue;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_money);
        //Get default NfcAdapter
        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        sharedpreferences = getSharedPreferences(Constant.MyPREFERENCES, Context.MODE_PRIVATE);
        userId = sharedpreferences.getString(Constant.ID,null);
        Bundle extras = getIntent().getExtras();
        queue = Volley.newRequestQueue(getApplicationContext());

        if(extras!=null){
            amount = extras.getString("amount");
        }
        // check NFC feature:
        if (nfcAdapter == null) {
            // process error device not NFC-capable…

        }

        //Check NDEF push (Beam) is activated
        if (!nfcAdapter.isNdefPushEnabled()) {
            // ask user to activate Beam option before:
            // startActivity(new Intent(Settings.ACTION_NFCSHARING_SETTINGS));
            // finish the activity:
            // finish();
        }

        // subscribe to the callback for sending Beam message:
        nfcAdapter.setNdefPushMessageCallback(this, this);
        // subscribe to the callback for the end of message receiving:
        nfcAdapter.setOnNdefPushCompleteCallback(this, this);
        // then, you must implement NfcAdapter.onNdefPushComplete…

    }

    @Override
    protected void onResume() {
        super.onResume();

        if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(getIntent().getAction())) {
            processIntent(getIntent());
        }
    }

    // implement the callback createNdefMessage
    public NdefMessage createNdefMessage(NfcEvent event) {

        if(amount==null){
            return null;
        }


        String msgTxt = sharedpreferences.getString(Constant.ID, null) +"_"+amount;
        byte[] lang = new byte[0];
        byte[] data = new byte[0];
        int langeSize = 0;

        try {
            lang = Locale.getDefault().getLanguage().getBytes("UTF-8");
            langeSize = lang.length;
            data = new byte[0];
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        try {
            data = msgTxt.getBytes("UTF-8");
            int dataLength = data.length;
            ByteArrayOutputStream payload = new ByteArrayOutputStream(dataLength);
            payload.write((byte) (dataLength & 0x1F));
            payload.write(data, 0, dataLength);
            ndefRecord = new NdefRecord(NdefRecord.TNF_WELL_KNOWN,
                    NdefRecord.RTD_TEXT, new byte[0],
                    payload.toByteArray());
            ndefMessage = new NdefMessage(ndefRecord);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return ndefMessage;
    }

    // implement the callback onNdefPushComplete
    public void onNdefPushComplete(NfcEvent arg0) {

        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
    }
    @Override
    public void onNewIntent(Intent intent) {
        if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(getIntent().getAction())) {
            processIntent(intent);
        }
    }
    void processIntent(Intent intent) {
        Parcelable[] rawMsgs = intent.getParcelableArrayExtra(
                NfcAdapter.EXTRA_NDEF_MESSAGES);
        // only one message sent during the beam
        NdefMessage msg = (NdefMessage) rawMsgs[0];
        String strMessage= new String(msg.getRecords()[0].getPayload());
        // record 0 contains the MIME type, record 1 is the AAR, if present
        String receiverId = strMessage.split("_")[0];
        receiverId=receiverId.trim();
        String amountTxt = strMessage.split("_")[1];
        amount = amountTxt;
        ProcessTrasaction(receiverId);

    }

    public void ProcessTrasaction(String receiverId){
        String url = "https://data-xchange.herokuapp.com/api/processTransaction/"+receiverId;
        User user= null;
        Map<String, String> params = new HashMap<String, String>();
        params.put("receiverId", userId);
        params.put("amount", amount);
        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, url,new JSONObject(params),
                new Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject responseJsonObject) {
                        // response
                        try {
                            String status=(String)responseJsonObject.get("status");
                            if(status.compareTo("success")==0){
                                Intent intent = new Intent(NDEFPushActivity.this, HomeActivity.class);
                                Toast toast = Toast.makeText(NDEFPushActivity.this.getApplicationContext(),  "Money received!", Toast.LENGTH_LONG);
                                toast.show();
                                startActivity(intent);
                            }
                        } catch (JSONException e) {
                            Log.v("JSonErr", e.getMessage());

                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        error.printStackTrace();
                        Log.e("Error.Response", String.valueOf(error.networkResponse.statusCode));
                    }
                }
        ) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/json");
                return params;
            }
        };
        queue.add(postRequest);
    }
}
